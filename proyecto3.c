#include <stdio.h>
#include <stdlib.h>
#include "help.h"//incluimos la biblioteca con las estructuras
// declaramos variables a ocupar
universidad proyecto[53];
carreras nombres[53];
int funcion;
//funcion que muestra al usuario las carreras o muestra la informacion, de una facultad previamente elegida
void print(int a, int b){
	int i;
	for(i=a;i<b;i++){
		if((funcion==1)||(funcion==2)){//mostramos todas las carreras de cierta facultad y su numero para elegir mas informacion de ella
			printf("%s %s %s %s %s %s %s %s %s \n",nombres[i].a,nombres[i].b,nombres[i].c,nombres[i].d,nombres[i].e,nombres[i].f,nombres[i].g,nombres[i].h,nombres[i].i);
		}
		else if(funcion==3){//mostramos todas las carreras de una facultad y sus ponderaciones.
			printf("%s	%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s \n ",nombres[i].c,nombres[i].d,nombres[i].e,nombres[i].f,nombres[i].g,nombres[i].h,nombres[i].i,proyecto[i].codigo,proyecto[i].nem,
			proyecto[i].ranking,proyecto[i].lenguaje,proyecto[i].matematicas,proyecto[i].historia,proyecto[i].ciencias,proyecto[i].ponderacion,proyecto[i].psu,proyecto[i].max,proyecto[i].min,proyecto[i].cupos,proyecto[i].bea);
		}
	}
}
void buscarfacultad(int funcion, universidad proyecto[]){//funcion que que busca las facultades y otras informaciones segun la opcion escogida en el menu.
	int var, i, var1, nem, rank, lengua, mate, histo, ciencias;//declaramos variables
	if(funcion==2){//si el usuario escogio la opcion de simular postulacion se le pide los datos necesarios.
		printf("ingrese los siguientes datos: nem, ranking, lenguaje, matematicas, historia, ciencias \n");
		printf("si no hizo alguna de las prueba simplemente ingrese 0.  ");
		scanf("%d %d %d %d %d %d" , &nem, &rank, &lengua, &mate, &histo, &ciencias);
	}
	printf(" elija el numero de su facultad: \n");//se le pide al usuario que elija un numero de las instrucciones
	printf(" 1 facultad de arquitectura \n 2 facultad de ciencias \n 3 facultad de ciencias del mar y recursos naturales \n 4 facultad de ciencias economicas y administrativas \n 5 facultad de derecho y ciencias sociales \n ");
	printf("6 facultad de farmacia \n 7 facultad de humanidades \n 8 facultad de ingenieria \n 9 facultad de medicina \n 10 facultad de odontologia \n ");
	scanf("%d" , &var);
	switch(var){//segun la facultad elegida entrega numeros de acuerdo al rango en que se encuentran las carreras de dicha facultad en el documento que creamos
		case 1: print(0,5);
			break;
		case 2: print(5, 9);
			break;
		case 3: if(funcion==1){//esta facultad solo tiene 1 carrera, por lo tanto se muestra directamente la informacion requerida por el usuario.
						printf("esta facultad solo tiene una carrera: biologia marina ");
						printf(" su ponderacion es= \n nem: %s \n ranking: %s \n lenguaje: %s \n matematicas: %s \n historia: %s \n ciencias: %s \n ultimo seleccionado: %s \n cupos: %s \n"
						,proyecto[9].nem,proyecto[9].ranking,proyecto[9].lenguaje,proyecto[9].matematicas,proyecto[9].historia,proyecto[9].ciencias,proyecto[9].min,proyecto[9].cupos);
						}
						else if(funcion==3){//la informacion varia dependiendo la opcion que elegio al principio, si quiere su ponderacion o la informacion de las carreras de cierta facultad
							printf("%s	%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s \n ",nombres[9].c,nombres[9].d,nombres[9].e,nombres[9].f,nombres[9].g,nombres[9].h,nombres[9].i,proyecto[9].codigo,proyecto[9].nem,
							proyecto[9].ranking,proyecto[9].lenguaje,proyecto[9].matematicas,proyecto[9].historia,proyecto[9].ciencias,proyecto[9].ponderacion,proyecto[9].psu,proyecto[9].max,proyecto[9].min,proyecto[9].cupos,proyecto[9].bea);
						}
			break;
		case 4: print(10,21);//lo mismo que la primera funcion, con diferentes rangos
			break;
		case 5: print(21, 23);
			break;
		case 6: print(23, 25);
			break;
		case 7: print(25, 29);
			break;
		case 8: print(29, 39);
			break;
		case 9: print(39, 52);
			break;
		case 10:if(funcion==1){//lo mismo que en el caso 3
				printf("esta facultad solo tiene una carrera: odontologia ");
				printf(" su ponderacion es= \n nem: %s \n ranking: %s \n lenguaje: %s \n matematicas: %s \n historia: %s \n ciencias: %s \n ultimo seleccionado: %s \n cupos: %s \n"
				,proyecto[52].nem,proyecto[52].ranking,proyecto[52].lenguaje,proyecto[52].matematicas,proyecto[52].historia,proyecto[52].ciencias,proyecto[52].min,proyecto[52].cupos);
				}
				else if(funcion==3){
					printf("%s	%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s \n ",nombres[52].c,nombres[52].d,nombres[52].e,nombres[52].f,nombres[52].g,nombres[52].h,nombres[52].i,proyecto[52].codigo,proyecto[52].nem,
					proyecto[52].ranking,proyecto[52].lenguaje,proyecto[52].matematicas,proyecto[52].historia,proyecto[52].ciencias,proyecto[52].ponderacion,proyecto[52].psu,proyecto[52].max,proyecto[52].min,proyecto[52].cupos,proyecto[52].bea);
				}
			break;
	}
	if((funcion==1)||(funcion==2)){//si el usuario escogio la opcion 1 o 2 en el menu, se necesita la informacion adicional que es escojer una carrera de las mostradas por la funcion anterior
		if((var!=3) && (var!=10)){
			printf("elija el numero de su carrera: \n");
			scanf("%d" , &var1 );//para mostrarla se le pide al usuario un numero que representa a la carrera segun las instruccions previas.
			//y se muestra al usuario mediantes estructuras
			printf(" su ponderacion es= \n nem: %s \n ranking: %s \n lenguaje: %s \n matematicas: %s \n historia: %s \n ciencias: %s \n ultimo seleccionado: %s \n cupos: %s \n"
			,proyecto[var1].nem,proyecto[var1].ranking,proyecto[var1].lenguaje,proyecto[var1].matematicas,proyecto[var1].historia,proyecto[var1].ciencias,proyecto[var1].min,proyecto[var1].cupos);
		}
	}
}
void abrirarchivo(char path[], universidad proyecto[]){//esta funcion abre el archivo con la informacion general de todas las carreras
	FILE *file;//esta basado de la funcion que ocuparon los profesores anteriores para los controles 5 y6
	int i = 0;//se decalran variables
	if((file=fopen(path,"r"))==NULL){//se comprueba que el archivo abrio correctamente
		printf("\n error al arbir el archivo");
		exit(0);
	}
	else{
		while (feof(file) == 0) {//si el archivo abrio de forma correcta, se guardan las diferentes palabras en la estructura declarada previamente
			fscanf(file,"%s	%s %s %s %s %s %s %s %s %s %s %s %s %s %s",proyecto[i].facultad,proyecto[i].carrera,proyecto[i].codigo,proyecto[i].nem,proyecto[i].ranking,proyecto[i].lenguaje,proyecto[i].matematicas,proyecto[i].historia,proyecto[i].ciencias,proyecto[i].ponderacion,
			proyecto[i].psu,proyecto[i].max,proyecto[i].min,proyecto[i].cupos,proyecto[i].bea);
			i++;
		}
		fclose(file);//se cierra el archivo
	}
}
void abrirnombres(char path[], carreras nombres[]){// esta funcion abre el archivo con el nombre completo de las carreras
		FILE *file;//ademas el archivo tiene instrucciones para mostrarles al usuario(escoger un numero para elegir la carrera correspondiente)
		int i = 0;
		if((file=fopen(path,"r"))==NULL){
			printf("\n error al arbir el archivo");
			exit(0);
		}
		else{
			while (feof(file) == 0) {//se guardan las palabras del archvio en otra estructura, diferente del archivo anterior
				fscanf(file,"%s	%s %s %s %s %s %s %s %s",nombres[i].a,nombres[i].b,nombres[i].c,nombres[i].d,nombres[i].e,nombres[i].f,nombres[i].g,nombres[i].h,nombres[i].i);
				i++;
			}
			fclose(file);//se cierra el archivo
		}
}

void consultarponderacion(universidad proyecto[]){//si el usuario escoje la opcion 1 del menu, viene a esta funcion.
	funcion = 1;//se asigna el numero de la funcion a la variable y se envia a la funcion que busca facultades
	buscarfacultad(funcion, proyecto);
}
void simularpostulacion(universidad proyecto[]){//si el usuario escoje la opcion 2 del menu, viene a esta funcion.
	funcion = 2;//se hace lo mismo que el primero, pero se busca informacion un poco diferente a la 1
	buscarfacultad(funcion, proyecto);
}
void mostrarponderacion(universidad proyecto[]){
	funcion = 3;//se hace lo mismo que el primero, pero finalmente mostrara todas las carreras de cierta facultad al usuario.
	buscarfacultad(funcion, proyecto);
}
void menu(universidad proyecto[]){// funcion con el menu
		int opcion;//se declaran variables y se muestran instrucciones al usuario
    do{
		printf( "\n   1. consultar ponderaciones de la carrera a elegir");
		printf( "\n   2. simular postulacion de la carrera a elegir");
		printf( "\n   3. Mostrar ponderaciones de la facultad a elegir");
		printf( "\n   4. Salir" );
		printf( "\n\n   Introduzca opción (1-4): ");
		scanf( "%d", &opcion);
    switch (opcion){//dependiendo de la opcion elegida se va adistintas funciones
			case 1: consultarponderacion(proyecto);
					break;
			case 2:  simularpostulacion(proyecto);
					break;
			case 3: mostrarponderacion(proyecto);
					break;     }
    } while (opcion != 4);
}
int main(){ // empieza el programa.
	abrirnombres("descripcion2.txt", nombres);//se leen los 2 archivos necesarios para este programa
	abrirarchivo("nuevo.txt", proyecto);
	menu(proyecto);//y se ejecuta el menu
	return EXIT_SUCCESS;//fin del programa
}
